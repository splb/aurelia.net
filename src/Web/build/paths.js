var appRoot = 'src/';
var outputRoot = 'dist/';
var exporSrvtRoot = 'wwwroot/'

module.exports = {
  root: appRoot,
  source: appRoot + '**/*.ts',
  html: appRoot + '**/*.html',
  css: [appRoot + '**/*.css'],
  sass: appRoot + '**/*.scss',
  style: 'styles/**/*.css',
  output: outputRoot,
  exportSrv: exporSrvtRoot,
  doc: './doc',
  e2eSpecsSrc: 'test/e2e/src/*.ts',
  e2eSpecsDist: 'test/e2e/dist/',
  dtsSrc: [
    'typings/**/*.ts', 
    './jspm_packages/**/*.d.ts'
  ]
}