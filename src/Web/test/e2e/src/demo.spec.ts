import {PageObject_App} from './app.po';
import {PageObject_BasicBinding} from './basic_binding.po';

describe('aurelia skeleton app', () => {
    let po_app: PageObject_App;
    let po_basicbinding: PageObject_BasicBinding;

    beforeEach(() => {
        po_app = new PageObject_App();
        po_basicbinding = new PageObject_BasicBinding();

        browser.loadAndWaitForAureliaPage("http://localhost:9000");
    });

    it('should load the page and display the initial page title', () => {
        expect(po_app.getCurrentPageTitle()).toBe('Hello, World! | Component Examples');
    });

    it('should navigate to api methods page', () => {
        po_app.navigateTo('#/apimethods');
        expect(po_app.getCurrentPageTitle()).toBe('API Methods | Component Examples');
    });
});
