import {App} from '../../src/app';

class RouterStub {
  routes: any;
  
  configure(handler) {
    handler(this);
  }
  
  map(routes) {
    this.routes = routes;
  }
}

describe('the app module', () => {
  var sut: App, mockedRouter: RouterStub;

  beforeEach(() => {
    mockedRouter = new RouterStub();
    sut = new App();
    sut.configureRouter(mockedRouter, mockedRouter);
  });
    
  it('contains a router property', () => {
    expect(sut.router).toBeDefined();
  });

  it('configures the router title', () => {
    expect(sut.router.title).toEqual("Component Examples");
  });

  it('should have a hello world route', () => {
      expect(sut.router.routes).toContain({ route: ['', 'helloworld'], name: 'hello_world', moduleId: 'routes/hello_world', nav: true, title: 'Hello, World!' });
  });

  it('should have a binding route', () => {
      expect(sut.router.routes).toContain({ route: ['binding'], name: 'basic_binding', moduleId: 'routes/basic_binding', nav: true, title: 'Basic Binding' });
  });

  it('should have an api methods route', () => {
      expect(sut.router.routes).toContain({ route: ['apimethods'], name: 'api_methods', moduleId: 'routes/api_methods', nav: true, title: 'API Methods' });
  });
});
