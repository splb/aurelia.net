export class BasicBindingController {
    heading = "Name Concatenator 5000!";
    firstName = "Donald";
    lastName = "Trump";
    previousValue = this.fullName;

    get fullName() {
        return `${this.firstName} ${this.lastName}`;
    }

    submit() {
        this.previousValue = this.fullName;
        alert(`Hello there, ${this.fullName}!`);
    }

    canDeactivate() {
        if (this.fullName !== this.previousValue) {
            return confirm("Don't go! You didn't press the submit button. That's the best bit.");
        }
    }
}