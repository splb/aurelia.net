﻿import {HttpClient} from "aurelia-http-client";
import {autoinject} from "aurelia-framework";
import {ActivityModel} from "models/activity";

@autoinject
export class ApiMethodsController {
    activity: ActivityModel;
    activities = [];
    spinner: any;

    constructor(private http: HttpClient) { }

    url = "https://aurelia-test.firebaseio.com/activities.json";

    attached() {
        this.loadActivities();
    }

    submit() {
        this.http.post(this.url, this.activity).then(() => this.loadActivities());
    }

    loadActivities() {
        this.spinner.classList.remove("hide");                                      // Show a little spinner to let people know stuff is happening.
        return this.http.get(this.url)
            .then(data => JSON.parse(data.response))                                // Parse the JSON string to a JS object
            .then(obj => this.activities = Object.keys(obj).map(key => obj[key]))   // Map the JS obj to an array
            .then(() => this.spinner.classList.add("hide"));                        // Hide the little spinner again. Stuff just happened.
    }
}