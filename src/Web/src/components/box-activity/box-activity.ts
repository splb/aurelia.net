﻿import {bindable} from "aurelia-framework";
import {ActivityModel} from "models/activity";

export class BoxActivity {
    @bindable activity: ActivityModel;

    Bookmark() {
        console.log("Bookmarked!");
    }

    AddToBasket() {
        console.log("Added to basket!");
    }
}