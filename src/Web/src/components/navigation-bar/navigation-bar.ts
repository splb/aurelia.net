﻿import * as $ from "jquery";

export class NavigationBar {
    toggleNavigation() {
        $("#collapse-menu").toggleClass("toggled");
    }
}