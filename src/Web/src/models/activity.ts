﻿declare module "models/activity" {
    export class ActivityModel {
        name: string;
        category: string;
        type: string;
        hours: string;
        description: string;
    }

}