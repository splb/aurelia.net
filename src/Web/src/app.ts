import {Router, RouterConfiguration} from 'aurelia-router'

export class App {
  router: Router;

  configureRouter(config: RouterConfiguration, router: Router) {
    config.title = 'Component Examples';
    config.map([
        { route: ['', 'helloworld'], name: 'hello_world', moduleId: 'routes/hello_world', nav: true, title: 'Hello, World!' },
        { route: ['binding'], name: 'basic_binding', moduleId: 'routes/basic_binding', nav: true, title: 'Basic Binding' },
        { route: ['apimethods'], name: 'api_methods', moduleId: 'routes/api_methods', nav: true, title: 'API Methods' }
    ]);

    this.router = router;
  }
}